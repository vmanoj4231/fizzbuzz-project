﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public interface IFizzBuzzLogic
    {
        List<string> FizzBuzzDetails(int Calcvalue);

    }
}
