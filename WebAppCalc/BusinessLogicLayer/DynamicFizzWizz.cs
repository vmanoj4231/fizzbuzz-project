﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Net;

namespace BusinessLogicLayer
{
   public class DayVerify: IDayVerification
    {
        private readonly string day;
        
        public DayVerify(string day)
        {
            this.day = day;
        }
        public bool daycalculation()
        {
            return DateTime.Now.DayOfWeek.ToString() == day;
        }
    }
}
