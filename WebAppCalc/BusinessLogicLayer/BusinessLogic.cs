﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructureMap;

namespace BusinessLogicLayer
{
    public class GetFizzBuzzData: IFizzBuzzLogic
    {
        private readonly IEnumerable<IDivisibleFizzBuzz> Objfizzbuzz;
        public GetFizzBuzzData(IEnumerable<IDivisibleFizzBuzz> Objfizzbuzzdata)
        {
            Objfizzbuzz = Objfizzbuzzdata;
        }
        public List<string> FizzBuzzDetails(int Calcvalue)
        {
                 
            int calc;

            var list = new List<string>();
         

            for (calc = 1; calc <= Calcvalue; calc++)
            {
                var fizzbuzzresult = this.Objfizzbuzz.Where(prop => prop.DivisibleHandler(calc));
                if (fizzbuzzresult.Any())
                {
                    list.Add(string.Join("", fizzbuzzresult.Select(x=>x.GetData())));
                }
                else{
                   list.Add(calc.ToString());
                }        

            }
            return list;
        }
    }



}

