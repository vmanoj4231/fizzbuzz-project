﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class DivisibleThreeFizz : IDivisibleFizzBuzz
    {
        private readonly IDayVerification dayVerify;

        public DivisibleThreeFizz(IDayVerification dayValidate)
        {
            this.dayVerify = dayValidate;
        }

        public bool DivisibleHandler(int number)
        {
            return number % 3 == 0;
        }

        public string GetData()
        {
            return dayVerify.daycalculation() ? "Wizz" : "Fizz";
        }
    }
}
