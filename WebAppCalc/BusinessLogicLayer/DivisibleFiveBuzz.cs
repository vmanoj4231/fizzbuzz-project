﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class DivisibleFiveBuzz : IDivisibleFizzBuzz
    {

        private readonly IDayVerification dayVerify;

        public DivisibleFiveBuzz(IDayVerification dayValidate)
        {
            this.dayVerify = dayValidate;
        }

        public bool DivisibleHandler(int number)
        {
            return number % 5 == 0;
        }

        public string GetData()
        {
            return dayVerify.daycalculation() ? "Wuzz" : "Buzz";
        }
    }
}
