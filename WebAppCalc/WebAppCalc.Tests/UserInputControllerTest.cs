﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using WebApp;
using WebApp.Controllers;
using WebApp.Models;
using BusinessLogicLayer;
using System.Web.Mvc;

namespace WebAppCalc.Tests
{

  [TestFixture]
  public  class UserInputControllerTest
    {
        private UserInputController userInputControllerObject;
        private Mock<IFizzBuzzLogic> mockFizzBuzzLogicObject;
        private UserInputModel userInputModelObject;
        private List<string> fizzbuzzListDetailsObject;

        [SetUp]
        public void SetUp()
        {
            mockFizzBuzzLogicObject = new Mock<IFizzBuzzLogic>();
            userInputControllerObject = new UserInputController(mockFizzBuzzLogicObject.Object);
            fizzbuzzListDetailsObject = new List<string>() { "1", "2", "Fizz", "4", "Buzz" };
        }

        [Test]
        public void UserInputController_viewpage()
        {
            var actualValue = userInputControllerObject.Index() as ViewResult;
            Assert.AreEqual(actualValue.ViewName, "Index");
        }

        [TestCase(3)]
        public void GetList_InputNumber_IsValid(int number)
        {
            int page = 1;
            userInputModelObject = new UserInputModel() { UserInput = number };
            mockFizzBuzzLogicObject.Setup(x => x.FizzBuzzDetails(number)).Returns(fizzbuzzListDetailsObject);
            var result = userInputControllerObject.Result(userInputModelObject, page) as ViewResult;
            Assert.AreEqual(result.Model, userInputModelObject);
            Assert.AreEqual(result.ViewName, "Index");
        }

        [TestCase(0)]
        [TestCase(1001)]
        public void ModelstateValidate_IsValid(int number)
        {
            int page = 1;
            userInputModelObject = new UserInputModel() { UserInput = number };
            userInputControllerObject.ModelState.AddModelError("Number", "Please enter a number between o and 1001");
            mockFizzBuzzLogicObject.Setup(x => x.FizzBuzzDetails(number)).Returns(fizzbuzzListDetailsObject);
            var result = userInputControllerObject.Result(userInputModelObject, page) as ViewResult;
            Assert.AreEqual(result.Model, userInputModelObject);
            Assert.AreEqual(result.ViewName, "Index");

            mockFizzBuzzLogicObject.Verify(x => x.FizzBuzzDetails(number), Times.Never);

        }

        [TearDown]
        public void TearDown()
        {
            mockFizzBuzzLogicObject = null;
            userInputControllerObject = null;
            fizzbuzzListDetailsObject = null;
        }
    } 
}
