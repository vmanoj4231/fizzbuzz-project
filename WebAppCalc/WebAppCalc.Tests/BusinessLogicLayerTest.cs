﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using WebApp;
using WebApp.Controllers;
using NUnit.Framework;
using BusinessLogicLayer;
using Moq;

namespace WebApp.Tests
{
    [TestFixture]
    public class BusinessLogicLayerTest
    {

        private BusinessLogic mockGetFizzBuzzObject;
        private List<IDivisibleFizzBuzz> mockDivisibleFizzBuzzOject;
        private Mock<IDivisibleFizzBuzz> divisibleThreeMock;
        private Mock<IDivisibleFizzBuzz> divisibleFiveMock;

        [SetUp]
        public void SetUp()
        {
            divisibleThreeMock = new Mock<IDivisibleFizzBuzz>();
            divisibleFiveMock  = new Mock<IDivisibleFizzBuzz>();
            mockDivisibleFizzBuzzOject = new List<IDivisibleFizzBuzz>() { divisibleThreeMock.Object, divisibleFiveMock.Object };
            mockGetFizzBuzzObject = new BusinessLogic(mockDivisibleFizzBuzzOject);
        }



        [TestCase(5)]
        [TestCase(10)]
        public void Get_List_of_FizzBuzz_Input_is_an_integer(int number)
        {
            var result = mockGetFizzBuzzObject.FizzBuzzDetails(number);
            Assert.IsInstanceOf<List<string>>(result);
            Assert.IsInstanceOf<int>(number);
        }

        [Test]
        public void Get_List_of_FizzBuzz_ModThreeFizz()
        {
            divisibleThreeMock.Setup(m => m.DivisibleHandler(3)).Returns(true);
            divisibleThreeMock.Setup(m => m.OutputData()).Returns("Wizz");
            var result = mockGetFizzBuzzObject.FizzBuzzDetails(3);

            Assert.AreEqual(result[0], "1");
            Assert.AreEqual(result[1], "2");
            Assert.AreEqual(result[2], "Wizz");
            divisibleThreeMock.Verify(m=>m.DivisibleHandler(It.IsAny<int>()),Times.Exactly(3));
        }

        [Test]
        public void Get_List_of_FizzBuzz_ModFiveBuzz()
        {
            divisibleThreeMock.Setup(m => m.DivisibleHandler(3)).Returns(true);
            divisibleThreeMock.Setup(m => m.OutputData()).Returns("Fizz");
            divisibleFiveMock.Setup(m => m.DivisibleHandler(5)).Returns(true);
            divisibleFiveMock.Setup(m => m.OutputData()).Returns("Buzz");
            var result = mockGetFizzBuzzObject.FizzBuzzDetails(5);

            Assert.AreEqual(result[0], "1");
            Assert.AreEqual(result[1], "2");
            Assert.AreEqual(result[2], "Fizz");
            Assert.AreEqual(result[3], "4");
            Assert.AreEqual(result[4], "Buzz");
            divisibleFiveMock.Verify(m => m.DivisibleHandler(It.IsAny<int>()), Times.Exactly(5));

        }

        [TearDown]
        public void TearDown()
        {
            divisibleThreeMock = null;
            divisibleFiveMock = null;
        }
    }
}
