﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using WebApp;
using WebApp.Controllers;
using NUnit.Framework;
using BusinessLogicLayer;
using Moq;


namespace WebApp.Tests
{
    [TestFixture]
    public class DivisibleThreeFizzTest
    {
        Mock<IDayVerification> mockObjectIDayVerification;
        [SetUp]
        public void Setup()
        {
            mockObjectIDayVerification = new Mock<IDayVerification>();
        }


        [TestCase(3,true)]
        [TestCase(7, false)]
        [TestCase(9, true)]
        [TestCase(10, false)]
        [TestCase(15, true)]

        public void If_Divisible_By_Three(int number,bool expectedObject)
        {
            var diviisbleByThreeObject = new DivisibleThreeFizz(mockObjectIDayVerification.Object);
            var actualValue = diviisbleByThreeObject.DivisibleHandler(number);
            Assert.That(actualValue, Is.EqualTo(expectedObject));

        }

        [Test]
        public void GetData_Check_Wizz_DayIsWednesday()
        {
            mockObjectIDayVerification.Setup(day => day.DayCheck()).Returns(true);
            var getDataObject = new DivisibleThreeFizz(mockObjectIDayVerification.Object);
            var expectedResult = getDataObject.OutputData();
            Assert.That(expectedResult, Is.EqualTo("Wizz"));

            mockObjectIDayVerification.Verify(day => day.DayCheck(), Times.Exactly(1));
        }

        [Test]
        public void GetData_Check_Fizz_DayIsWednesday()
        {
            mockObjectIDayVerification.Setup(day => day.DayCheck()).Returns(true);
            var getDataObject = new DivisibleThreeFizz(mockObjectIDayVerification.Object);
            var expectedResult = getDataObject.OutputData();
            Assert.That(expectedResult, Is.EqualTo("Fizz"));

            mockObjectIDayVerification.Verify(day => day.DayCheck(), Times.Once);
        }

        [TearDown]
        public void TearDown()
        {
            mockObjectIDayVerification = null;
        }

    }
}
