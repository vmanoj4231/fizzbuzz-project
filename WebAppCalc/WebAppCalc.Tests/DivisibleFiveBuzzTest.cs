﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp;
using WebApp.Controllers;
using NUnit.Framework;
using BusinessLogicLayer;
using Moq;

namespace WebApp.Tests
{
    [TestFixture]
    public class DivisibleFiveBuzzTest
    {
        
        Mock<IDayVerification> mockObjectIDayVerification;
        [SetUp]
        public void Setup()
        {
            mockObjectIDayVerification = new Mock<IDayVerification>();
        }


        [TestCase(5, true)]
        [TestCase(6, false)]
        [TestCase(9, false)]
        [TestCase(10, true)]
        [TestCase(15, true)]

        public void If_Divisible_By_Five(int number,bool expectedObject)
        {
            var diviisbleByThreeObject = new DivisibleFiveBuzz(mockObjectIDayVerification.Object);
            var actualValue = diviisbleByThreeObject.DivisibleHandler(number);
            Assert.That(actualValue, Is.EqualTo(expectedObject));

        }

        [Test]
        public void GetData_Check_Wizz_DayIsWednesday()
        {
            mockObjectIDayVerification.Setup(day => day.DayCheck()).Returns(true);
            var getDataObject = new DivisibleFiveBuzz(mockObjectIDayVerification.Object);
            var expectedResult = getDataObject.OutputData();
            Assert.That(expectedResult, Is.EqualTo("Wuzz"));

            mockObjectIDayVerification.Verify(day => day.DayCheck(), Times.Exactly(1));
        }

        [Test]
        public void GetData_Check_Fizz_DayIsWednesday()
        {
            mockObjectIDayVerification.Setup(day => day.DayCheck()).Returns(true);
            var getDataObject = new DivisibleFiveBuzz(mockObjectIDayVerification.Object);
            var expectedResult = getDataObject.OutputData();
            Assert.That(expectedResult, Is.EqualTo("Buzz"));

            mockObjectIDayVerification.Verify(day => day.DayCheck(), Times.Once);
        }

        [TearDown]
        public void TearDown()
        {
            mockObjectIDayVerification = null;
        }

    }
}
