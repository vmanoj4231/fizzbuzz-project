﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using BusinessLogicLayer;

namespace WebApp.Tests
{
    [TestFixture]
    public class DayVerificationTest
    {
        private string day;
        private Mock<DayVerify> mockObjectIDayVerification;

        [SetUp]
        public void Setup()
        {
            mockObjectIDayVerification = new Mock<DayVerify>();
        }

        [Test]
        public void Validateday_should_TodaysDay()
        {
            day = DayOfWeek.Wednesday.ToString();
            var dayVerifyObject = new DayVerify(day);
            var actualValue = dayVerifyObject.DayCheck();
            Assert.That(actualValue, Is.EqualTo("Wednesday"));
        }

        [TearDown]
        public void TearDown()
        {
            day = null;
        }
    }
}
