﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using BusinessLogicLayer;
using PagedList;

namespace WebAppCalc.Models
{
    public class CalcModel
    {
        [Required]
        [RegularExpression("^[0-9]*$",ErrorMessage="Number must be numeric")]
        [Range(1, 1000)]
        public int Calcvalue { get; set; }

        public IPagedList <string> result {get;set;}


    }


       
}
