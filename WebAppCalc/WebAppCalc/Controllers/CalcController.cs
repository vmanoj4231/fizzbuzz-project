﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppCalc.Models;
using BusinessLogicLayer;
using PagedList;

namespace WebAppCalc.Controllers
{
    public class CalcController : Controller
    {
        private readonly IFizzBuzzLogic FizzBuzzLogic;
        public CalcController(IFizzBuzzLogic FizzBuzzLogic)
        {
            this.FizzBuzzLogic = FizzBuzzLogic;
        }
        // GET: Calc
        public ActionResult Index()
        {
            return View();
        }

     
        public ActionResult Result(CalcModel calculator, int? page)
        {
            if (ModelState.IsValid)
            {
                int pageNumber = page != null ? Convert.ToInt32(page) : 1;
                int pageSize = 10;
                var fizzbuzzListDetails = this.FizzBuzzLogic.FizzBuzzDetails(calculator.Calcvalue);
                if (fizzbuzzListDetails != null)
                {
                    calculator.result = fizzbuzzListDetails.ToPagedList(pageNumber, pageSize);
                }
                
                
            }           
            return View("Index", calculator);
        }


    }
}